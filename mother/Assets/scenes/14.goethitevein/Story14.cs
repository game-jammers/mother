//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using Cinemachine;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class Story14
        : Story
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public TextAsset beforeComplete                         = null;
        public TextAsset afterComplete                          = null;
        public Drone drone                                      = null;
        
        //
        // story methods //////////////////////////////////////////////////////
        //
        
        protected override IEnumerator Begin()
        {
            storyman.lockInteract = true;

            SetDisruptState();

            if(!flags[ProgressFlag.Complete_Disruptor]
                && flags[ProgressFlag.Has_NaniteLens] 
                && flags[ProgressFlag.Has_NeumannCatalyst]
            )
            {
                yield return Complete();
            }

            storyman.lockInteract = false;
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator Complete()
        {
            flags[ProgressFlag.Complete_Disruptor] = true;

            yield return PlayDialog(dialog, beforeComplete);

            SetDisruptState();

            yield return PlayDialog(dialog, afterComplete);
        }
        
        //
        // ------------------------------------------------------------------------
        //
        
        private void SetDisruptState()
        {
            drone.gameObject.SetActive(flags[ProgressFlag.Complete_Disruptor]);
        }
    }
}

