//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class Drone
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public float wanderRadius;
        private Vector3 startPos;

        public Transform shootTarget;
        private Vector3 moveTarget;

        public Vector2 moveDelay                                = new Vector2(1f, 2f);
        public RandomizeLine line                              = null;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            startPos = transform.position;
            UpdateMoveTarget();

            StartCoroutine(MoveAround());
        }

        protected virtual void Update()
        {
            line.startpos = transform.position;
            line.endpos = shootTarget.position;
        }

        private void UpdateMoveTarget()
        {
            moveTarget = new Vector3(
                btRandom.Range(0f, 1f),
                btRandom.Range(0f, 1f),
                btRandom.Range(0f, 1f)
            ) * wanderRadius + startPos;
        }

        private IEnumerator MoveAround()
        {
            while(true)
            {
                Vector3 diff = moveTarget - transform.position;
                float dist2 = diff.sqrMagnitude;

                transform.position = Vector3.Slerp(transform.position, moveTarget, Time.deltaTime);

                if(dist2 <= 1f)
                {
                    UpdateMoveTarget();
                    yield return new WaitForSeconds(btRandom.Range(moveDelay.x, moveDelay.y));
                }

                yield return new WaitForEndOfFrame();
            }
        }
    }
}
