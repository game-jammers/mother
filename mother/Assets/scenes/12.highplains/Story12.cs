//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using Cinemachine;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class Story12
        : Story
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public TextAsset beforeComplete                         = null;
        public TextAsset afterComplete                          = null;
        public GameObject accelerator                           = null;
        
        //
        // story methods //////////////////////////////////////////////////////
        //
        
        protected override IEnumerator Begin()
        {
            SetAccelState();

            storyman.lockInteract = true;

            if(!flags[ProgressFlag.Complete_GiapsisAccelerator]
                && flags[ProgressFlag.Has_RosenAlloy] 
                && flags[ProgressFlag.Has_ParticleAccelerator]
                && flags[ProgressFlag.Has_LorentzConduit]
            )
            {
                yield return Complete();
            }

            storyman.lockInteract = false;

        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator Complete()
        {
            flags[ProgressFlag.Complete_GiapsisAccelerator] = true;

            yield return PlayDialog(dialog, beforeComplete);

            SetAccelState();

            yield return PlayDialog(dialog, afterComplete);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void SetAccelState()
        {
            accelerator.SetActive(flags[ProgressFlag.Complete_GiapsisAccelerator]);
        }
    }
}

