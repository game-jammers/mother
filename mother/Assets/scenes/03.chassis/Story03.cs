//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class Story03
        : Story
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("FX")]
        public List<GameObject> fireFX                          = null;
        public Interact geneticsLabTravel                       = null;
        
        //
        // story methods //////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            if(flags[ProgressFlag.ShutOffPlasma])
            {
                foreach(GameObject go in fireFX)
                {
                    Destroy(go);
                }

                geneticsLabTravel.gameObject.SetActive(true);
            }
        }
        
        protected override IEnumerator Begin()
        {
            yield break;
        }  
    }
}

