//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class Story00
        : Story
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Controls")]
        public btCamera sceneCam                                = null;
        public UIElement curtain                                = null;
        public UIElement titleCard                              = null;
        public UIElement titleText                              = null;
        public AudioSource music                                = null;

        [Header("Timelines")]
        public PlayableDirector start;
        public PlayableDirector crash;

        [Header("FX")]
        public Transform starfield                              = null;
        public GameObject atmosphereFx                          = null;
        public Color crashColor                                 = Color.red;

        [Header("Dialog")]
        public TextAsset iAmArgosJson;

        //
        // story methods //////////////////////////////////////////////////////
        //
        
        protected override IEnumerator Begin()
        {
            start.Play();

            yield return new WaitForSeconds(1f);
            
            var wait = new WaitForSeconds(0.1f);
            yield return curtain.HideAsync(1f);

            yield return PlayDialog(dialog, iAmArgosJson);

            start.Stop();
            crash.Play();

            Skybox skybox = sceneCam.skybox;

            const float duration = 6f;
            yield return AnimateSkybox(skybox, duration, crashColor, Color.black, -1f);

            yield return titleCard.ShowAsync(3.0f);

            StartCoroutine(FadeAudio(music, 0f, 4f));

            yield return new WaitForSeconds(2f);

            yield return titleText.HideAsync(3f);

            GameManager.ChangeScene(SceneId.Awakening);
        }
    }
}
