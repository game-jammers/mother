//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using Cinemachine;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class Story19
        : Story
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Dialog")]
        public UIElement curtain                                = null;
        public TextAsset ending                                 = null;
        public TextAsset ending2                                = null;
        public UIElement endCredits                             = null;

        [Header("Effects")]
        public AudioSource music                                = null;

        [Header("Day / Night Cycle")]
        public Color skyStartColorDay;
        public Color skyEndColorDay;
        public Color skyStartColorNight;
        public Color skyEndColorNight;

        public Color groundStartColorDay;
        public Color groundEndColorDay;
        public Color groundStartColorNight;
        public Color groundEndColorNight;

        public float dayCycleTime                               = 1f;
        public float healCycleTime                              = 30f;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void SaveHumanity()
        {
            GameManager.ChangeScene(SceneId.SaveHumanity);
        }

        //
        // story methods //////////////////////////////////////////////////////
        //
        
        protected override IEnumerator Begin()
        {
            yield return new WaitForSeconds(1f);

            music.Play();

            StartCoroutine(DayNightCycle());

            yield return curtain.HideAsync(1f);

            yield return PlayDialog(dialog, ending);

            yield return new WaitForSeconds(3.0f);

            yield return PlayDialog(dialog, ending2);

            yield return endCredits.ShowAsync(1f);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerator DayNightCycle()
        {
            Skybox skybox = SceneManager.instance.sceneCam.skybox;
            float healElapsed = 0f;
            float daytimeElapsed = 0f;

            var wait = new WaitForEndOfFrame();

            while(true)
            {
                healElapsed += Time.deltaTime;
                daytimeElapsed += Time.deltaTime;

                float healPerc = Mathf.Clamp(healElapsed / healCycleTime, 0f, 1f);
                float daytimePerc = Mathf.PingPong(daytimeElapsed / dayCycleTime, 1f);

                Color daySky = Color.Lerp(skyStartColorDay, skyEndColorDay, healPerc);
                Color nightSky = Color.Lerp(skyStartColorNight, skyEndColorNight, healPerc);
                Color dayGround = Color.Lerp(groundStartColorDay, groundEndColorDay, healPerc);
                Color nightGround = Color.Lerp(groundStartColorNight, groundEndColorNight, healPerc);

                Color sky = Color.Lerp(daySky, nightSky, daytimePerc);
                Color ground = Color.Lerp(dayGround, nightGround, daytimePerc);

                SetSkybox(skybox, ground, sky);
                yield return wait;
            }
        }

        
    }
}

