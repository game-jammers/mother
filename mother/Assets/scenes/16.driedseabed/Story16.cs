//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using Cinemachine;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class Story16
        : Story
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public TextAsset beforeComplete                         = null;
        public TextAsset afterComplete                          = null;
        public GameObject reactor                               = null;
        
        //
        // story methods //////////////////////////////////////////////////////
        //
        
        protected override IEnumerator Begin()
        {
            storyman.lockInteract = true;

            SetReactionState();

            if(!flags[ProgressFlag.Complete_ReactionChamber]
                && flags[ProgressFlag.Has_GravitonField] 
                && flags[ProgressFlag.Has_FieldBalancer]
            )
            {
                yield return Complete();
            }
            
            storyman.lockInteract = false;
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator Complete()
        {
            flags[ProgressFlag.Complete_ReactionChamber] = true;

            yield return PlayDialog(dialog, beforeComplete);

            SetReactionState();

            yield return PlayDialog(dialog, afterComplete);
        }
        
        //
        // --------------------------------------------------------------------
        //

        private void SetReactionState()
        {
            reactor.SetActive(flags[ProgressFlag.Complete_ReactionChamber]);
        }
    }
}

