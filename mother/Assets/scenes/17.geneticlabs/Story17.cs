//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using Cinemachine;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class Story17
        : Story
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Dialog")]
        public TextAsset complete                               = null;
        public AudioSource choiceAudio                          = null;

        [Header("Choice")]
        public TextAsset choice                                 = null;
        public UIDialog choiceDialog                            = null;

        [Header("DEBUG")]
        public bool justDoIt                                    = false;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void SaveMother()
        {
            GameManager.ChangeScene(SceneId.SaveMother);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void SaveHumanity()
        {
            GameManager.ChangeScene(SceneId.SaveHumanity);
        }

        //
        // story methods //////////////////////////////////////////////////////
        //
        
        protected override IEnumerator Begin()
        {
            if(flags[ProgressFlag.Complete_LehmannDrive]
                && flags[ProgressFlag.Complete_GiapsisAccelerator] 
                && flags[ProgressFlag.Complete_Disruptor]
                && flags[ProgressFlag.Complete_ReactionChamber]
                && !flags[ProgressFlag.Complete_Choice]
            )
            {
                yield return Complete();
            }
            else if(justDoIt)
            {
                yield return Complete();
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator Complete()
        {
            storyman.lockInteract = true;

            choiceAudio.Play();

            yield return PlayDialog(dialog, complete);

            yield return PlayDialog(choiceDialog, choice);

            storyman.lockInteract = false;
            switch(choiceDialog.lastResponse)
            {
                case UIDialog.Response.Confirm:
                    GameManager.ChangeScene(SceneId.SaveHumanity);
                    break;
                case UIDialog.Response.Cancel:
                    GameManager.ChangeScene(SceneId.SaveMother);
                    break;
            }

        }
        
    }
}

