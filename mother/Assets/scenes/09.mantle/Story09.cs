//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using Cinemachine;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class Story09
        : Story
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public TextAsset beforeComplete                         = null;
        public TextAsset afterComplete                          = null;

        public GameObject oldCore                               = null;
        public GameObject hotCore                               = null;
        
        //
        // story methods //////////////////////////////////////////////////////
        //
        
        protected override IEnumerator Begin()
        {
            SetCoreState();

            storyman.lockInteract = true;
            if(!flags[ProgressFlag.Complete_LehmannDrive]
                && flags[ProgressFlag.Has_Antimatter] 
                && flags[ProgressFlag.Has_TharpCrystals]
            )
            {
                yield return Complete_LehmannDrive();
            }
            
            storyman.lockInteract = false;
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator Complete_LehmannDrive()
        {
            flags[ProgressFlag.Complete_LehmannDrive] = true;

            yield return PlayDialog(dialog, beforeComplete);

            SetCoreState();
            yield return new WaitForSeconds(0.5f);

            yield return PlayDialog(dialog, afterComplete);
        }

        //
        // --------------------------------------------------------------------
        //

        private void SetCoreState()
        {
            oldCore.SetActive(!flags[ProgressFlag.Complete_LehmannDrive]);
            hotCore.SetActive(flags[ProgressFlag.Complete_LehmannDrive]);
        }
        
        
    }
}

