//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using Cinemachine;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class Story01
        : Story
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Dialog")]
        public UIDialog dialogBlack;
        public TextAsset awaken01;

        [Header("Interface")]
        public UIElement curtain;

        [Header("Cameras")]
        public CinemachineVirtualCamera probeCloseup;
        public CinemachineVirtualCamera wideShot;

        //
        // story methods //////////////////////////////////////////////////////
        //
        
        protected override IEnumerator Begin()
        {
            storyman.lockInteract = true;

            probeCloseup.Priority = 10;
            wideShot.Priority = 0;

            yield return new WaitForSeconds(1.0f);

            yield return PlayDialog(dialogBlack, awaken01);

            yield return curtain.HideAsync(1f);

            yield return new WaitForSeconds(2.0f);

            probeCloseup.Priority = 0;
            wideShot.Priority = 10;
            
            storyman.lockInteract = false;
        }
    }
}

