//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public abstract class Story
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public class DialogLines
        {
            public DialogLine[] lines;
        }

        [System.Serializable]
        public class DialogLine
        {
            private const float kDefaultSpeed                   = 0.8f;

            [TextArea(5,10)] public string text;
            public string title;
            public bool speedOverride;
            [ShowIf("speedOverride")] public float speed;

            public UIDialog.Request ToRequest()
            {
                return new UIDialog.Request() {
                    title = new UITextType.Paragraph() {
                        text = title,
                        speed = 0f
                    },
                    body = new UITextType.Paragraph[] {
                        new UITextType.Paragraph() { text = text, speed = speedOverride ? speed : kDefaultSpeed }
                    }
                };
            }
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public StorySceneManager storyman                       { get; private set; }
        public UserManager user                                 { get { return GameManager.instance.user; } }
        public ProgressFlagState flags                          { get { return user.flags; } }
        public bool firstVisited                                { get; private set; }

        [Header("Story")]
        public UIDialog dialog                                  = null;
        public TextAsset firstDialog                            = null;


        //
        // static methods /////////////////////////////////////////////////////
        //
        
        public static DialogLines LoadLines(TextAsset json)
        {
            return JsonUtility.FromJson<DialogLines>(json.text);
        }

        //
        // --------------------------------------------------------------------
        //

        public static IEnumerator PlayDialog(UIDialog dialog, TextAsset linesJson)
        {
            Dbg.Assert(dialog != null, "Dialog is null?");
            var wait = new WaitForSeconds(0.1f);
            DialogLines lines = LoadLines(linesJson);
            foreach(DialogLine line in lines.lines)
            {
                Dbg.Assert(line != null, "Dialog line is null?");
                yield return dialog.OpenAsync(line.ToRequest());
                while(dialog.isActive) yield return wait;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static IEnumerator PlayDialog(UIDialog dialog, DialogLine line)
        {
            var wait = new WaitForSeconds(0.1f);
            yield return dialog.OpenAsync(line.ToRequest());
            while(dialog.isActive) yield return wait;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static IEnumerator FadeAudio(AudioSource src, float end, float duration)
        {
            float start = src.volume;

            Dbg.Assert(duration > 0f, "Cannot fade 0 my dude");
            float dt = 0f;
            while(dt < duration)
            {
                float perc = dt / duration;
                src.volume = Mathf.Lerp(start, end, perc);
                yield return new WaitForEndOfFrame();

                dt += Time.deltaTime;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static IEnumerator AnimateSkybox(Skybox skybox, float duration, Color newGroundColor, Color newSkyColor, float newOffset)
        {
            // don't overwrite the official skybox
            Material skymat = new Material(skybox.material);
            skybox.material = skymat;

            Color gcol = skybox.material.GetColor("_GroundColor");
            Color scol = skybox.material.GetColor("_SkyColor");
            float voff = skybox.material.GetFloat("_VerticalOffset");
            float dt = 0f;
            while(dt < duration)
            {
                float perc = dt/duration;
                Color ngcol = Color.Lerp(gcol, newGroundColor, perc);
                Color nscol = Color.Lerp(scol, newSkyColor, perc);
                skybox.material.SetColor("_GroundColor", ngcol);
                skybox.material.SetColor("_SkyColor", nscol);
                skybox.material.SetFloat("_VerticalOffset", Mathf.Lerp(voff, newOffset, perc));
                yield return new WaitForEndOfFrame();
                dt += Time.deltaTime;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public static void SetSkybox(Skybox skybox, Color sky, Color ground)
        {
            skybox.material.SetColor("_GroundColor", sky);
            skybox.material.SetColor("_SkyColor", ground);
        }
        

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public IEnumerator Begin(StorySceneManager manager)
        {

            firstVisited = false;
            if(user.visited.Contains(name) == false)
            {
                firstVisited = true;
                user.visited.Add(name);
            }

            storyman = manager;
            storyman.lockInteract = true;
            yield return GameManager.instance.FadeIn();
            if(firstVisited && dialog != null && firstDialog != null)
            {
                yield return PlayDialog(dialog, firstDialog);
            }

            yield return Begin();
            storyman.lockInteract = false;
        }

        //
        // --------------------------------------------------------------------
        //

        protected abstract IEnumerator Begin();

        //
        // --------------------------------------------------------------------
        //

        public virtual void Signal(string data)
        {
        }
        
    }
}
