//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class SceneManager
        : BaseSceneManager
    {
        // events //////////////////////////////////////////////////////////////
        public delegate void SceneReadyCallback();
        public event SceneReadyCallback OnSceneReady;

        // members /////////////////////////////////////////////////////////////
        public static new SceneManager instance                 { get; private set; }

        // unity callbacks /////////////////////////////////////////////////////
        protected override void Awake()
        {
            base.Awake();
            instance = this;
            GameManager.EnsureExists();
        }

        // protected methods ///////////////////////////////////////////////////
        protected void NotifySceneReady()
        {
            if( OnSceneReady != null )
            {
                OnSceneReady();
            }
        }
    }
}
