//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class StorySceneManager
         : SceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Interface")]
        public Image cursor                                     = null;
        private Sprite cursorDefaultIcon                        = null;
        public Color cursorDefaultColor                         = Color.white;
        public TextMeshProUGUI cursorHint                       = null;
        
        [Header("Story")]
        [SerializeField] Story story                            = null;

        [Header("Runtime")]
        [ReadOnly] public Interact activeInteract                          = null;
        [ReadOnly] public bool lockInteract                                = false;


        //
        // public methods /////////////////////////////////////////////////////
        //

        //
        // private methods ////////////////////////////////////////////////////
        //

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Start()
        {
            cursorDefaultIcon = cursor.sprite;
            if(story != null)
            {
                StartCoroutine(story.Begin(this));
            }
            else
            {
                StartCoroutine(GameManager.instance.FadeIn());
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void Update()
        {
            Vector2 canvasPoint = sceneCam.ScreenToCanvasPoint(Input.mousePosition.ToVector2XY());
            cursor.rectTransform.localPosition = canvasPoint.ToVector3XY();

            if(!lockInteract)
            {
                activeInteract = sceneCam.MousePick<Interact>();
                if(activeInteract != null)
                {
                    cursor.color = activeInteract.cursorColor;
                    if(activeInteract.cursorIcon != null)
                    {
                        cursor.sprite = activeInteract.cursorIcon;
                    }

                    if(cursorHint != null)
                    {
                        cursorHint.text = activeInteract.hintText;
                    }

                    if(Input.GetKeyDown(KeyCode.Mouse0))
                    {
                        StartCoroutine(Trigger(activeInteract));
                    }
                }
                else
                {
                    cursor.color = cursorDefaultColor;
                    cursor.sprite = cursorDefaultIcon;
                    if(cursorHint != null)
                    {
                        cursorHint.text = System.String.Empty;
                    }
                }
            }
        }

        //
        // ////////////////////////////////////////////////////////////////////////
        //
        
        private IEnumerator Trigger(Interact interact)
        {
            Dbg.Assert(interact != null, "Interact is null?");
            lockInteract = true;
            yield return interact.Trigger();
            yield return new WaitForSeconds(0.5f);
            lockInteract = false;
        }
    }
}
