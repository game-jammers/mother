//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public class UserManager
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public ProgressFlagState flags                          = new ProgressFlagState();
        
        public List<string> visited                             = new List<string>();
    }
}
