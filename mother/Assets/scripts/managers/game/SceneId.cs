//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public enum SceneId
    {
        StartScene = 0,
        Awakening = 1,
        Gully = 2,
        Chassis = 3,
        ImpactZone = 4,
        Cave = 5,
        Ravine = 6,
        Caverns = 7,
        ReactorCore = 8,
        MantleShelf = 9,
        ArterialConduits = 10,
        SensorArray = 11,
        HighPlains = 12,
        MountainPass = 13,
        GoethiteVein = 14,
        DriveBay = 15,
        DriedSeaBed = 16,
        GeneticLabs = 17,
        SaveHumanity = 18,
        SaveMother = 19,
        MemoryRoom = 20
    }
}
