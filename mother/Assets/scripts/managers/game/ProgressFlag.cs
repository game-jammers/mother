//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    public enum ProgressFlag
    {
        Has_Antimatter,
        Has_TharpCrystals,
        Complete_LehmannDrive,

        Has_RosenAlloy,
        Has_ParticleAccelerator,
        Has_LorentzConduit,
        Complete_GiapsisAccelerator,

        Has_NaniteLens,
        Has_NeumannCatalyst,
        Complete_Disruptor,
               
        Has_GravitonField,
        Has_FieldBalancer,
        Complete_ReactionChamber,
                
        ShutOffPlasma,
        Complete_Choice
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    [System.Serializable]
    public class ProgressFlagState
        : EnumList<ProgressFlag, bool>
    {
        public ProgressFlagState()
        {
            items.Fill(false);
        }
    }
}
