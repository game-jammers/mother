//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class GameManager
        : BaseGameManager
    {
        //
        // constants //////////////////////////////////////////////////////////
        //
        
        public static readonly string kPrefabPath               = "managers/GameManager";

        //
        // accessors ////////////////////////////////////////////////////////////
        //

        public static new GameManager instance                  { get; private set; }

        //
        // members ////////////////////////////////////////////////////////////
        //

        public UIElement curtain                                = null;

        public UserManager user                                 = null;
        
        public GameSettings settings                            = GameSettings.Default;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public static GameManager EnsureExists()
        {
            if( instance == null )
            {
                instance = EnsureExists<GameManager>( kPrefabPath );
                instance.name = "GameManager";
            }

            return instance;
        }

        //
        // --------------------------------------------------------------------
        //

        public static void ChangeScene( SceneId scene )
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync( (int)scene, UnityEngine.SceneManagement.LoadSceneMode.Single );
        }

        //
        // --------------------------------------------------------------------
        //

        public static void AddScene( SceneId scene )
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync( (int)scene, UnityEngine.SceneManagement.LoadSceneMode.Additive );
        }

        //
        // --------------------------------------------------------------------
        //

        public void ApplySettings()
        {
            ApplySettings(settings);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ApplySettings(GameSettings settings)
        {
            this.settings = settings;
            Cursor.lockState = settings.mouse.cursorMode;
            Cursor.visible = settings.mouse.visible;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void CaptureCursor()
        {
            settings.mouse.cursorMode = CursorLockMode.Locked;
            settings.mouse.visible = false;
            ApplySettings();
        }

        //
        // --------------------------------------------------------------------
        //

        public void ReleaseCursor()
        {
            settings.mouse.cursorMode = CursorLockMode.None;
            settings.mouse.visible = true;
            ApplySettings();
        }

        //
        // --------------------------------------------------------------------
        //

        public IEnumerator FadeOut(float duration = 0.5f)
        {

            if(duration <= 0f)
            {
                audio.SetFloat("MasterVolume", 0f);
                curtain.Show();
                yield break;
            }

            curtain.Show(duration, null);

            float master = 1f;
            float elapsed = 0f;
            audio.GetFloat("MasterVolume", out master);
            while(elapsed < duration)
            {
                yield return new WaitForEndOfFrame();
                elapsed += Time.deltaTime;
                float perc = elapsed / duration;

                audio.SetFloat("MasterVolume", Mathf.Lerp(0f, -20f, perc));
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public IEnumerator FadeIn(float duration = 0.5f)
        {
            if(duration <= 0f)
            {
                curtain.Hide();
                yield break;
            }

            curtain.Hide(duration, null);

            float master = 1f;
            float elapsed = 0f;
            audio.GetFloat("MasterVolume", out master);
            while(elapsed < duration)
            {
                yield return new WaitForEndOfFrame();
                elapsed += Time.deltaTime;
                float perc = elapsed / duration;

                audio.SetFloat("MasterVolume", Mathf.Lerp(-20f, 0f, perc));
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected override void Awake()
        {
            base.Awake();
            ApplySettings();
        }
    };
}
