//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEditor;

namespace blacktriangles
{
    public class ExampleDb
        : DatabaseTable
    {
        //
        // members/////////////////////////////////////////////////////////////////
        //
        
        [ShowInInspector] public GameManager[] managers         { get; private set; }
        
        //
        // public methods /////////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
           managers = LoadAll<GameManager>("managers"); 
        }
    }
}
