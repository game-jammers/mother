//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace blacktriangles
{
    public class Interact
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Interface")]
        public string hintText                                  = System.String.Empty;        
        public Sprite cursorIcon                                = null;
        public Color cursorColor                                = Color.white;

        [Header("Scene Transition")]
        public bool hasSceneTransition                          = false;
        [ShowIf("hasSceneTransition")] public SceneId targetScene;

        [Header("Dialog")]
        public bool hasDialog                                   = false;
        [ShowIf("hasDialog")] public UIDialog dialog            = null;
        [ShowIf("hasDialog")] public TextAsset dialogLines      = null;

        [Header("Signal")]
        public bool hasSignal                                   = false;
        [ShowIf("hasSignal")] public Story story                = null;
        [ShowIf("data")] public string data                     = System.String.Empty;

        [Header("Progress Flag")]
        public bool hasProgressFlag                             = false;
        [ShowIf("hasProgressFlag")] public ProgressFlag flag    = ProgressFlag.Has_Antimatter;
        [ShowIf("hasProgressFlag")] public bool isSet           = true;
        [ShowIf("hasProgressFlag")] public bool hideIfSet       = true;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public IEnumerator Trigger()
        {
            if(hasSceneTransition)
            {
                yield return GameManager.instance.FadeOut();
                GameManager.ChangeScene(targetScene);
            }
            else
            {
                if(hasDialog && dialogLines != null)
                {
                    yield return Story.PlayDialog(dialog, dialogLines);
                }

                if(hasSignal && story != null)
                {
                    story.Signal(data);
                }

                if( hasProgressFlag )
                {
                    GameManager.instance.user.flags[flag] = isSet;
                    if(hideIfSet)
                    {
                        Destroy(gameObject);
                    }
                }
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            if(hasProgressFlag && hideIfSet && GameManager.instance.user.flags[flag])
            {
                Destroy(gameObject);
            }
        }
        
    }
}
